package com.beshenov.lesson3.activityabcd.common;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.beshenov.lesson3.activityabcd.R;

public abstract class ActivityAB extends AppCompatActivity {
    protected       Button               mNextButton;
    protected final View.OnClickListener mOnClickListener;

    public ActivityAB() {
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClicked();
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ab);
        mNextButton = (Button) findViewById(R.id.nextButton);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mNextButton.setOnClickListener(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mNextButton.setOnClickListener(mOnClickListener);
    }

    protected abstract void onNextButtonClicked();
}

package com.beshenov.lesson3.activityabcd;

import android.content.Intent;

import com.beshenov.lesson3.activityabcd.common.ActivityAB;

public class ActivityB extends ActivityAB {

    @Override
    protected void onNextButtonClicked() {
        Intent intent = new Intent(this, ActivityC.class);
        startActivity(intent);
    }
}

package com.beshenov.lesson3.activityabcd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ActivityD extends AppCompatActivity {
    protected       Button               mGoToActivityBButton;
    protected final View.OnClickListener mOnGotoBClickListener;

    public ActivityD() {
        mOnGotoBClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityD.this, ActivityB.class);
                startActivity(intent);
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d);
        mGoToActivityBButton = (Button) findViewById(R.id.goToActivityBButton);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoToActivityBButton.setOnClickListener(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoToActivityBButton.setOnClickListener(mOnGotoBClickListener);
    }
}

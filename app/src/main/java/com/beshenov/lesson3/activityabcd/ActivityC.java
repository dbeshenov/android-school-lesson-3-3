package com.beshenov.lesson3.activityabcd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ActivityC extends AppCompatActivity {
    protected       Button               mNextButton;
    protected       Button               mGoToActivityBButton;
    protected final View.OnClickListener mOnNextClickListener;
    protected final View.OnClickListener mOnGotoBClickListener;

    public ActivityC() {
        mOnNextClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityC.this, ActivityD.class);
                startActivity(intent);
            }
        };

        mOnGotoBClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityC.this, ActivityB.class);
                startActivity(intent);
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c);
        mNextButton = (Button) findViewById(R.id.nextButton);
        mGoToActivityBButton = (Button) findViewById(R.id.goToActivityBButton);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mNextButton.setOnClickListener(mOnNextClickListener);
        mGoToActivityBButton.setOnClickListener(mOnGotoBClickListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mNextButton.setOnClickListener(null);
        mGoToActivityBButton.setOnClickListener(null);
    }
}

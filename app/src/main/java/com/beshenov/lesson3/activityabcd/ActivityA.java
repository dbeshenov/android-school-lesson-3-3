package com.beshenov.lesson3.activityabcd;

import android.content.Intent;

import com.beshenov.lesson3.activityabcd.common.ActivityAB;

public class ActivityA extends ActivityAB {
    @Override
    protected void onNextButtonClicked() {
        Intent intent = new Intent(this, ActivityB.class);
        startActivity(intent);
    }
}
